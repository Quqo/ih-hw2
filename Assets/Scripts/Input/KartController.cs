using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KartController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Vector3 startPos;
    private Rigidbody playerRb;

    private float horizontalInput;
    private float verticalInput;
    private float currentSteerAngle;
    private float currentbreakForce;
    private bool isBreaking;
    private bool isAccelerating;

    [SerializeField] private float motorForce;
    [SerializeField] private float breakForce;
    [SerializeField] private float maxSteerAngle;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float accelerationForce;

    [SerializeField] private Transform centerOfMass;

    [SerializeField] private WheelCollider frontLeftWheelCollider;
    [SerializeField] private WheelCollider frontRightWheelCollider;
    [SerializeField] private WheelCollider rearLeftWheelCollider;
    [SerializeField] private WheelCollider rearRightWheelCollider;

    [SerializeField] private Transform frontLeftWheelTransform;
    [SerializeField] private Transform frontRightWheeTransform;
    [SerializeField] private Transform rearLeftWheelTransform;
    [SerializeField] private Transform rearRightWheelTransform;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();

        playerRb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        startPos = transform.position;
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        playerRb.centerOfMass = transform.InverseTransformPoint(centerOfMass.position);

        GetInput();
        HandleMotor();
        HandleSteering();
        UpdateWheels();
        print(playerRb.velocity.magnitude);
    }


    private void GetInput()
    {
        horizontalInput = playerInputActions.Kart.Move.ReadValue<Vector2>().x;
        verticalInput = playerInputActions.Kart.Move.ReadValue<Vector2>().y;
        isBreaking = playerInputActions.Kart.Break.inProgress;
        isAccelerating = playerInputActions.Kart.Acceleration.inProgress;
    }

    private void HandleMotor()
    {
        frontLeftWheelCollider.motorTorque = verticalInput * motorForce;
        frontRightWheelCollider.motorTorque = verticalInput * motorForce;
        currentbreakForce = isBreaking ? breakForce : 0f;
        ApplyBreaking();

        //Not the right way of accelerating, but the intended effect is there
        if (isAccelerating)
        {
            Vector3 acelVector = new Vector3(
                playerRb.velocity.x * accelerationForce, 
                0, 
                playerRb.velocity.z * accelerationForce);

            Vector3 acceleratedVelocity = playerRb.velocity + acelVector.normalized;
            playerRb.velocity = acceleratedVelocity;
        }
        playerRb.velocity = Vector3.ClampMagnitude(playerRb.velocity, maxSpeed);
    }

    private void ApplyBreaking()
    {
        frontRightWheelCollider.brakeTorque = currentbreakForce;
        frontLeftWheelCollider.brakeTorque = currentbreakForce;
        rearLeftWheelCollider.brakeTorque = currentbreakForce;
        rearRightWheelCollider.brakeTorque = currentbreakForce;
    }

    private void HandleSteering()
    {
        currentSteerAngle = maxSteerAngle * horizontalInput;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    private void UpdateWheels()
    {
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheeTransform);
        UpdateSingleWheel(rearRightWheelCollider, rearRightWheelTransform);
        UpdateSingleWheel(rearLeftWheelCollider, rearLeftWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Vector3 pos;
        Quaternion rot; 
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }

    private void ResetPosition()
    {
        playerRb.MovePosition(startPos);
        playerRb.MoveRotation(Quaternion.identity);

        frontLeftWheelCollider.motorTorque = 0f;
        frontRightWheelCollider.motorTorque = 0f;
        playerRb.velocity = Vector3.zero;
    }
}
