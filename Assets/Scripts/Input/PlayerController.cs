using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody playerRb;
    [SerializeField] private float moveSpeed = 600f;
    private Vector3 startPos;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();
        playerRb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        startPos = transform.position;
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 moveDirection = playerInputActions.Kart.Move.ReadValue<Vector2>();
        Move(moveDirection);
    }

    private void Move(Vector2 direction)
    {
        playerRb.velocity = new Vector3(
            direction.x * moveSpeed * Time.fixedDeltaTime, 
            0, 
            direction.y * moveSpeed * Time.fixedDeltaTime);
    }

    private void ResetPosition()
    {
        playerRb.MovePosition(startPos);
        playerRb.MoveRotation(Quaternion.identity);
    }
}
